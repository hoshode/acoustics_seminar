# README #

This repo is a student work done on Acoustics Seminar course in Spring 2016. The main topic is GPGPU processing using CUDA API, case Finite Difference Time Domain (FDTD) method. The created paper can be found as a part of the repository at *doc/akuseminar_CUDA.pdf*.

### What is this repository for? ###

* The program runs different CPU and GPU implementations and reports execution speeds of the computation process.
* Also includes a short OpenGL code part for debugging FDTD implementation.

### How do I get set up? ###

To properly run the program, you need

* Visual Studio (tested on VS2013)
* CUDA enabled graphics card (compute capability 3.0 or higher)
* CUDA 7.0

Because the program is for simple demonstration purposes, the code is not unit tested.
#pragma once

#include "testSpecs.h"

void testFDTDGpu();
//void testFDTDGpuTwoPoints();
void testFDTDGpuTexture();
void testFDTDGpuTexture2D();
void testFDTDGpuManaged();
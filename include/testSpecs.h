#pragma once
// use graphics (0 = off, 1 = on)
#define USE_GL 0
// simulation definitions
#define N_TEST_RUNS 1
#define N_STEPS 4410u
// room definitions
#define ROOM_WIDTH 5.5f
#define ROOM_HEIGHT 7.0f
#define XI 0.5f
// FDTD definitions
#define GRID_PRECISION 0.01f
#define SAMPLING_FREQUENCY 44100.0f
// sound source definitions
#define SOUND_SOURCE_X 2.0f
#define SOUND_SOURCE_Y 2.0f
#define IMPULSE_STRENGTH 50.0f

#pragma once

#include <vector>

// OpenGL stuff
#define GLEW_STATIC
#include "GL/glew.h"
#include "glfw3.h"

#include "testSpecs.h"

struct GLData;

typedef float State_t;
typedef std::vector<float> State;

void testFDTDCpu(GLData& gl);
void testFDTDCpuParallel(GLData& gl);

void computeFDTD(const State& old_state, const State& current_state, State& next_state, int i_x, int i_y, int grid_width, int grid_height, float div_xi);
void computeFDTDParallel(const State& old_state, const State& current_state, float* next_state, int i_x, int i_y, int grid_width, int grid_height, float div_xi);


GLFWwindow* initGL();
GLFWwindow* createWindow();
GLuint compileShaders();
void setupRoomGraphics(GLData& gl);
void draw(GLData& gl, State& s);
void glTeardown();
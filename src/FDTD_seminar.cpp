// FDTD_seminar.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "FDTD_seminar.h"

#include <algorithm>
#include <chrono>
#include <iostream>
#include <math.h>
#include <omp.h>
#include <string>

#include "FDTD_seminar_GPU.h"

namespace
{
	inline int stateIdx(int x, int y, int grid_width) { return x + y * grid_width; }
}

struct GLData
{
	GLData()
	: window(), program(), VAO(), VBO(), buffer_size()
	{ }

	void initialize()
	{
		window = initGL();
		program = compileShaders();
		setupRoomGraphics(*this);
	}

	GLFWwindow* window;
	GLuint program;
	GLuint VAO;  // Vertex Array Object
	GLuint VBO;  // Vertex Buffer Object
	GLuint EBO;  // Element Array Object
	GLuint VPO;  // Vertex Pressure Object
	int buffer_size;
};


void test_implementation(const std::string& impl_name, void(&impl)(GLData&), GLData& gl)
{
	long long total_duration = 0, run_duration = 0;
	auto t1 = std::chrono::high_resolution_clock::now();
	auto t2 = t1;
	std::cout << impl_name << ": " << std::endl;
	for (auto i = 0; i < N_TEST_RUNS; ++i)
	{
		t1 = std::chrono::high_resolution_clock::now();
		impl(gl);
		t2 = std::chrono::high_resolution_clock::now();
		run_duration = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count();
		total_duration += run_duration;
		std::cout << run_duration << "ms" << std::endl;
	}
	if (N_TEST_RUNS > 1)
	{
		std::cout << "Average: " << double(total_duration) / N_TEST_RUNS << " ms" << std::endl;
	}
}


void test_implementation(const std::string& impl_name, void(&impl)())
{
	long long total_duration = 0, run_duration = 0;
	auto t1 = std::chrono::high_resolution_clock::now();
	auto t2 = t1;
	std::cout << impl_name << ": " << std::endl;
	for (auto i = 0; i < N_TEST_RUNS; ++i)
	{
		t1 = std::chrono::high_resolution_clock::now();
		impl();
		t2 = std::chrono::high_resolution_clock::now();
		run_duration = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count();
		total_duration += run_duration;
		std::cout << run_duration << "ms" << std::endl;
	}
	if (N_TEST_RUNS > 1)
	{
		std::cout << "Average: " << double(total_duration) / N_TEST_RUNS << " ms" << std::endl;
	}
}


int _tmain(int argc, _TCHAR* argv[])
{
	GLData gl;
#if USE_GL
	gl.initialize();
	if (gl.window == nullptr)
	{
		std::system("pause");
		glTeardown();
		return -1;
	}
#endif

	std::cout << "Timing comparison between the CPU and the GPU." << std::endl;
	std::cout << "Start timing..." << std::endl;
	// Run test functions
	test_implementation("CPU", testFDTDCpu, gl);
	test_implementation("CPU (parallel)", testFDTDCpuParallel, gl);
	test_implementation("GPU", testFDTDGpu);
	test_implementation("GPU (texture)", testFDTDGpuTexture);
	test_implementation("GPU (texture 2D)", testFDTDGpuTexture2D);
	test_implementation("GPU (managed memory)", testFDTDGpuManaged);

	// end process
	std::system("pause");
#if USE_GL
	glTeardown();
#endif
	return 0;
}


void testFDTDCpu(GLData& gl)
{
	// run speedtest on the CPU
	const int grid_width = int(ROOM_WIDTH / GRID_PRECISION);
	const int grid_height = int(ROOM_HEIGHT / GRID_PRECISION);
	const int source_x = int(SOUND_SOURCE_X / GRID_PRECISION);
	const int source_y = int(SOUND_SOURCE_Y / GRID_PRECISION);
	State_t div_xi = 1.0 / (std::sqrt(2.0) * XI);
	// create grids
	State old_state(grid_width * grid_height, 0.0);
	State current_state(old_state);
	State next_state(current_state);
	// set impulse to the current state
	current_state[stateIdx(source_x, source_y, grid_width)] = IMPULSE_STRENGTH;
	for (auto step = 0u; step < N_STEPS; ++step)
	{
		//std::cout << step << "/" << N_STEPS << "\r" << std::flush;
		for (int i = 0; i < grid_width; ++i)
		{
			for (int j = 0; j < grid_height; ++j)
			{
				computeFDTD(old_state, current_state, next_state, i, j, grid_width, grid_height, div_xi);
			}
		}
		// take one step forward
		old_state.swap(current_state);
		current_state.swap(next_state);
#if USE_GL
		// update window
		draw(gl, current_state);
#endif
	}
	// output step number
	//std::cout << N_STEPS << "/" << N_STEPS << " ...ready." << std::endl;
}


void testFDTDCpuParallel(GLData& gl)
{
	// run speedtest on the CPU
	const int grid_width = int(ROOM_WIDTH / GRID_PRECISION);
	const int grid_height = int(ROOM_HEIGHT / GRID_PRECISION);
	const int source_x = int(SOUND_SOURCE_X / GRID_PRECISION);
	const int source_y = int(SOUND_SOURCE_Y / GRID_PRECISION);
	State_t div_xi = 1.0 / (std::sqrt(2.0) * XI);
	// create grids
	State old_state(grid_width * grid_height, 0.0);
	State current_state(old_state);
	State next_state(current_state);
	// set impulse to the current state
	current_state[stateIdx(source_x, source_y, grid_width)] = IMPULSE_STRENGTH;
	State_t local_state[grid_width];
	for (auto step = 0u; step < N_STEPS; ++step)
	{
		//std::cout << step << "/" << N_STEPS << "\r" << std::flush;
		#pragma omp parallel for nowait private(local_state)
		for (int j = 0; j < grid_height; ++j)
		{
			for (int i = 0; i < grid_width; ++i)
			{
				computeFDTDParallel(old_state, current_state, local_state, i, j, grid_width, grid_height, div_xi);
			}
			std::copy(&local_state[0], &local_state[grid_width], next_state.begin() + j * grid_width);
		}
		// take one step forward
		old_state.swap(current_state);
		current_state.swap(next_state);
#if USE_GL
		// update window
		draw(gl, current_state);
#endif
	}
	// output step number
	//std::cout << N_STEPS << "/" << N_STEPS << " ...ready." << std::endl;
}



void computeFDTD(const State& old_state, const State& current_state, State& next_state, 
				 int i_x, int i_y, int grid_width, int grid_height, State_t div_xi)
{
	State_t el_old = old_state[stateIdx(i_x, i_y, grid_width)];
	State_t div = 1.0;
	State_t el = -el_old;
	// compute FDTD scheme
	if (i_x > 0)
	{
		el += 0.5f * current_state[stateIdx(i_x - 1, i_y, grid_width)];
	}
	else
	{
		el += 0.5f * current_state[stateIdx(i_x + 1, i_y, grid_width)] + el_old * div_xi;
		div += div_xi;
	}
	if (i_y > 0)
	{
		el += 0.5f * current_state[stateIdx(i_x, i_y - 1, grid_width)];
	}
	else
	{
		el += 0.5f * current_state[stateIdx(i_x, i_y + 1, grid_width)] + el_old * div_xi;
		div += div_xi;
	}
	if ((i_x + 1) < grid_width)
	{
		el += 0.5f * current_state[stateIdx(i_x + 1, i_y, grid_width)];
	}
	else
	{
		el += 0.5f * current_state[stateIdx(i_x - 1, i_y, grid_width)] + el_old * div_xi;
		div += div_xi;
	}
	if ((i_y + 1) < grid_height)
	{
		el += 0.5f * current_state[stateIdx(i_x, i_y + 1, grid_width)];
	}
	else
	{
		el += 0.5f * current_state[stateIdx(i_x, i_y - 1, grid_width)] + el_old * div_xi;
		div += div_xi;
	}
	// write result to the output array
	next_state[stateIdx(i_x, i_y, grid_width)] = el / div;
}


void computeFDTDParallel(const State& old_state, const State& current_state, float* next_state,
	int i_x, int i_y, int grid_width, int grid_height, float div_xi)
{
	float el_old = old_state[stateIdx(i_x, i_y, grid_width)];
	float div = 1.0;
	float el = -el_old;
	// compute FDTD scheme
	if (i_x > 0)
	{
		el += 0.5f * current_state[stateIdx(i_x - 1, i_y, grid_width)];
	}
	else
	{
		el += 0.5f * current_state[stateIdx(i_x + 1, i_y, grid_width)] + el_old * div_xi;
		div += div_xi;
	}
	if (i_y > 0)
	{
		el += 0.5f * current_state[stateIdx(i_x, i_y - 1, grid_width)];
	}
	else
	{
		el += 0.5f * current_state[stateIdx(i_x, i_y + 1, grid_width)] + el_old * div_xi;
		div += div_xi;
	}
	if ((i_x + 1) < grid_width)
	{
		el += 0.5f * current_state[stateIdx(i_x + 1, i_y, grid_width)];
	}
	else
	{
		el += 0.5f * current_state[stateIdx(i_x - 1, i_y, grid_width)] + el_old * div_xi;
		div += div_xi;
	}
	if ((i_y + 1) < grid_height)
	{
		el += 0.5f * current_state[stateIdx(i_x, i_y + 1, grid_width)];
	}
	else
	{
		el += 0.5f * current_state[stateIdx(i_x, i_y - 1, grid_width)] + el_old * div_xi;
		div += div_xi;
	}
	// write result to the output array
	next_state[i_x] = el / div;
}


GLFWwindow* initGL()
{
	// GLFW init
	glfwInit();
	auto* window = createWindow();
	auto ret = glewInit();
	if (ret != GLEW_OK)
	{
		std::cerr << "Failed to initialize GLEW: " << glewGetErrorString(ret) << std::endl;
		return nullptr;
	}
	return window;
}


GLFWwindow* createWindow()
{
	// window specs
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	// GLEW init
	glewExperimental = GL_TRUE;
	GLFWwindow* window = glfwCreateWindow(800, 600, "FDTD debugger", nullptr, nullptr);
	if (window == nullptr)
	{
		std::cerr << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return nullptr;
	}
	glfwMakeContextCurrent(window);
	glViewport(0, 0, 800, 600);

	return window;
}


GLuint compileShaders()
{
	// create vertex and fragment shaders
	char* vertex_source =
		"#version 440 core \n"
		"in vec3 position; \n"
		"in float pressure; \n"
		"out vec3 p; \n"
		"void main() \n"
		"{ \n"
		"	p = vec3(clamp(2.0f * pressure, 0.0f, 1.0f), \n"
		"			 clamp(-2.0f * pressure, 0.0f, 1.0f), \n"
		"			 2.0f * (clamp(abs(pressure), 0.5f, 1.0f) - 0.5f)); \n"
		"	gl_Position = vec4(position, 1.0); \n"
		"}";
	GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertex_shader, 1, &vertex_source, NULL);
	glCompileShader(vertex_shader);
	GLint success;
	GLchar infoLog[512];
	glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(vertex_shader, 512, NULL, infoLog);
		std::cerr << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
	}
	char* fragment_source =
		"#version 440 core \n"
		"in vec3 p; \n"
		"out vec4 color; \n"
		"void main() \n"
		"{ \n"
		"	color = vec4(p, 1.0f); \n"
		"}";
	GLuint fragment_shader;
	fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment_shader, 1, &fragment_source, NULL);
	glCompileShader(fragment_shader);
	glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(fragment_shader, 512, NULL, infoLog);
		std::cerr << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
	}
	// setup shader program
	GLuint shaderProgram;
	shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertex_shader);
	glAttachShader(shaderProgram, fragment_shader);

	glBindAttribLocation(shaderProgram, 0, "position");
	glBindAttribLocation(shaderProgram, 1, "pressure");

	glLinkProgram(shaderProgram);
	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(shaderProgram, 512, NULL, infoLog);
		std::cerr << "ERROR::SHADER::PROGRAM::LINK_FAILED\n" << infoLog << std::endl;
	}
	glUseProgram(shaderProgram);
	// shader objects are not needed -> remove
	glDeleteShader(vertex_shader);
	glDeleteShader(fragment_shader);
	return shaderProgram;
}


void setupRoomGraphics(GLData& gl)
{
	const int grid_width = int(ROOM_WIDTH / GRID_PRECISION);
	const int grid_height = int(ROOM_HEIGHT / GRID_PRECISION);
	float div = float(std::max(grid_width, grid_height));
	auto n_points = grid_width * grid_height;
	std::vector<GLfloat> points(n_points * 3, 0.0f);
	for (auto i = 0u; i < n_points; ++i)
	{
		// range [0, height (or width)]
		auto i_x = (i % grid_width);
		auto i_y = (i / grid_width);
		// range [0, 1 (or height/width)]
		auto x = i_x / div;
		auto y = i_y / div;
		// final range [-1, 1]
		points[3*i] = 2.0f * (x - 0.5f);
		points[3*i + 1] = 2.0f * (y - 0.5f);
	}
	std::vector<GLuint> indices;
	for (auto i = 0u; i < grid_width - 1; ++i)
	{
		for (auto j = 0u; j < grid_height - 1; ++j)
		{
			// upper triangle
			indices.push_back(stateIdx(i, j, grid_width));
			indices.push_back(stateIdx(i+1, j, grid_width));
			indices.push_back(stateIdx(i, j+1, grid_width));
			// lower triangle
			indices.push_back(stateIdx(i+1, j, grid_width));
			indices.push_back(stateIdx(i+1, j+1, grid_width));
			indices.push_back(stateIdx(i, j+1, grid_width));
		}
	}

	gl.buffer_size = indices.size();
	// generate buffers
	glGenVertexArrays(1, &gl.VAO);
	glGenBuffers(1, &gl.VBO);
	glGenBuffers(1, &gl.EBO);
	glGenBuffers(1, &gl.VPO);

	glBindVertexArray(gl.VAO);
	glBindBuffer(GL_ARRAY_BUFFER, gl.VBO);
	glBufferData(GL_ARRAY_BUFFER, points.size() * sizeof(GLfloat), &points[0], GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, gl.VPO);
	glBufferData(GL_ARRAY_BUFFER, points.size() * sizeof(GLfloat), &State(points.size(), 0.0f)[0], GL_STREAM_DRAW);
	glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gl.EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), &indices[0], GL_STATIC_DRAW);
	glBindVertexArray(0);
}

void draw(GLData& gl, State& s)
{
	// Clear the colorbuffer
	glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	glUseProgram(gl.program);

	glBindBuffer(GL_ARRAY_BUFFER, gl.VPO);
	glBufferData(GL_ARRAY_BUFFER, s.size() * sizeof(float), &s[0], GL_STREAM_DRAW);
	// draw
	glBindVertexArray(gl.VAO);
	glDrawElements(GL_TRIANGLES, gl.buffer_size, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
	glfwSwapBuffers(gl.window);
}

void glTeardown()
{
	glfwTerminate();
}

#include "FDTD_seminar_GPU.h"

#include <cuda.h>
#include <cuda_runtime.h>
#include <math.h>
#include <iostream>
#include <vector>

typedef float CUDA_State_t;

typedef CUDA_State_t* CUDA_State;

__device__ __inline__ int stateIdxGPU(int x, int y, int width) { return x + y * width; }

__device__ __inline__ CUDA_State stateEl2D(CUDA_State state, int pitch, int x, int y) 
{ 
	return (CUDA_State)((char*)state + y * pitch) + x;
}

__global__ void FDTDGPU(CUDA_State old_state, CUDA_State current_state, CUDA_State next_state,
						int width, int height, CUDA_State_t div_xi)
{
	int i_x = threadIdx.x + blockIdx.x * blockDim.x;
	int i_y = threadIdx.y + blockIdx.y * blockDim.y;

	if (i_x < width && i_y < height)
	{
		CUDA_State_t el_old = old_state[stateIdxGPU(i_x, i_y, width)];
		CUDA_State_t div = 1.0;
		CUDA_State_t el = -el_old;
		CUDA_State_t coeff = 0.5;
		// compute FDTD scheme
		if (i_x > 0)
		{
			el += coeff * current_state[stateIdxGPU(i_x - 1, i_y, width)];
		}
		else
		{
			el += coeff * current_state[stateIdxGPU(i_x + 1, i_y, width)] + el_old * div_xi;
			div += div_xi;
		}
		if (i_y > 0)
		{
			el += coeff * current_state[stateIdxGPU(i_x, i_y - 1, width)];
		}
		else
		{
			el += coeff * current_state[stateIdxGPU(i_x, i_y + 1, width)] + el_old * div_xi;
			div += div_xi;
		}
		if ((i_x + 1) < width)
		{
			el += coeff * current_state[stateIdxGPU(i_x + 1, i_y, width)];
		}
		else
		{
			el += coeff * current_state[stateIdxGPU(i_x - 1, i_y, width)] + el_old * div_xi;
			div += div_xi;
		}
		if ((i_y + 1) < height)
		{
			el += coeff * current_state[stateIdxGPU(i_x, i_y + 1, width)];
		}
		else
		{
			el += coeff * current_state[stateIdxGPU(i_x, i_y - 1, width)] + el_old * div_xi;
			div += div_xi;
		}
		// write result to the output array
		next_state[stateIdxGPU(i_x, i_y, width)] = el / div;
	}
}


__global__ void FDTDGPU_Texture(cudaTextureObject_t old_state, cudaTextureObject_t current_state, CUDA_State next_state,
								int width, int height, CUDA_State_t div_xi)
{
	int i_x = threadIdx.x + blockIdx.x * blockDim.x;
	int i_y = threadIdx.y + blockIdx.y * blockDim.y;

	if (i_x < width && i_y < height)
	{
		CUDA_State_t el_old = tex1Dfetch<CUDA_State_t>(old_state, stateIdxGPU(i_x, i_y, width));
		CUDA_State_t div = 1.0;
		CUDA_State_t el = -el_old;
		CUDA_State_t coeff = 0.5;
		// compute FDTD scheme
		if (i_x > 0)
		{
			el += coeff * tex1Dfetch<CUDA_State_t>(current_state, stateIdxGPU(i_x - 1, i_y, width));
		}
		else
		{
			el += coeff * tex1Dfetch<CUDA_State_t>(current_state, stateIdxGPU(i_x + 1, i_y, width)) + el_old * div_xi;
			div += div_xi;
		}
		if ((i_x + 1) < width)
		{
			el += coeff * tex1Dfetch<CUDA_State_t>(current_state, stateIdxGPU(i_x + 1, i_y, width));
		}
		else
		{
			el += coeff * tex1Dfetch<CUDA_State_t>(current_state, stateIdxGPU(i_x - 1, i_y, width)) + el_old * div_xi;
			div += div_xi;
		}
		if (i_y > 0)
		{
			el += coeff * tex1Dfetch<CUDA_State_t>(current_state, stateIdxGPU(i_x, i_y - 1, width));
		}
		else
		{
			el += coeff * tex1Dfetch<CUDA_State_t>(current_state, stateIdxGPU(i_x, i_y + 1, width)) + el_old * div_xi;
			div += div_xi;
		}
		if ((i_y + 1) < height)
		{
			el += coeff * tex1Dfetch<CUDA_State_t>(current_state, stateIdxGPU(i_x, i_y + 1, width));
		}
		else
		{
			el += coeff * tex1Dfetch<CUDA_State_t>(current_state, stateIdxGPU(i_x, i_y - 1, width)) + el_old * div_xi;
			div += div_xi;
		}
		// write result to the output array
		next_state[stateIdxGPU(i_x, i_y, width)] = el / div;
	}
}


__global__ void FDTDGPU_Texture2D(cudaTextureObject_t old_state, cudaTextureObject_t current_state, CUDA_State next_state,
	int width, int height, CUDA_State_t div_xi)
{
	int i_x = threadIdx.x + blockIdx.x * blockDim.x;
	int i_y = threadIdx.y + blockIdx.y * blockDim.y;

	if (i_x < width && i_y < height)
	{
		CUDA_State_t el_old = tex2D<CUDA_State_t>(old_state, i_x, i_y);
		CUDA_State_t div = 1.0;
		CUDA_State_t el = -el_old;
		CUDA_State_t coeff = 0.5;
		// compute FDTD scheme
		if (i_x > 0)
		{
			el += coeff * tex2D<CUDA_State_t>(current_state, i_x - 1, i_y);
		}
		else
		{
			el += coeff * tex2D<CUDA_State_t>(current_state, i_x + 1, i_y) + el_old * div_xi;
			div += div_xi;
		}
		if ((i_x + 1) < width)
		{
			el += coeff * tex2D<CUDA_State_t>(current_state, i_x + 1, i_y);
		}
		else
		{
			el += coeff * tex2D<CUDA_State_t>(current_state, i_x - 1, i_y) + el_old * div_xi;
			div += div_xi;
		}
		if (i_y > 0)
		{
			el += coeff * tex2D<CUDA_State_t>(current_state, i_x, i_y - 1);
		}
		else
		{
			el += coeff * tex2D<CUDA_State_t>(current_state, i_x, i_y + 1) + el_old * div_xi;
			div += div_xi;
		}
		if ((i_y + 1) < height)
		{
			el += coeff * tex2D<CUDA_State_t>(current_state, i_x, i_y + 1);
		}
		else
		{
			el += coeff * tex2D<CUDA_State_t>(current_state, i_x, i_y - 1) + el_old * div_xi;
			div += div_xi;
		}
		// write result to the output array
		next_state[stateIdxGPU(i_x, i_y, width)] = el / div;
	}
}


__global__ void initState(CUDA_State state1, CUDA_State state2, CUDA_State state3, int x, int y, int width, int height)
{
	int i_x = threadIdx.x + blockIdx.x * blockDim.x;
	int i_y = threadIdx.y + blockIdx.y * blockDim.y;

	if (i_x < width && i_y < height)
	{
		state1[stateIdxGPU(i_x, i_y, width)] = CUDA_State_t(0.0);
		// set source
		if (i_x == x && i_y == y)
		{
			state2[stateIdxGPU(x, y, width)] = CUDA_State_t(IMPULSE_STRENGTH);
		}
		else
		{
			state2[stateIdxGPU(i_x, i_y, width)] = CUDA_State_t(0.0);
		}
		state3[stateIdxGPU(i_x, i_y, width)] = CUDA_State_t(0.0);
	}
}


__global__ void initState2D(CUDA_State state1, CUDA_State state2, CUDA_State state3, int x, int y, int pitch, int width, int height)
{
	int i_x = threadIdx.x + blockIdx.x * blockDim.x;
	int i_y = threadIdx.y + blockIdx.y * blockDim.y;
	int byte_width = width * sizeof(CUDA_State_t);

	if (i_x < width && i_y < height)
	{
		// set source
		*stateEl2D(state1, pitch, i_x, i_y) = CUDA_State_t(0.0);
		if (i_x == x && i_y == y)
		{
			*stateEl2D(state2, pitch, i_x, i_y) = CUDA_State_t(IMPULSE_STRENGTH);
		}
		else
		{
			*stateEl2D(state2, pitch, i_x, i_y) = CUDA_State_t(0.0);
		}
		*stateEl2D(state3, pitch, i_x, i_y) = CUDA_State_t(0.0);

	}
}



void testFDTDGpu()
{

	// run speedtest on the GPU
	const int grid_width = int(ROOM_WIDTH / GRID_PRECISION);
	const int grid_height = int(ROOM_HEIGHT / GRID_PRECISION);
	const int n_elements = grid_width * grid_height;
	const int n_bytes = n_elements * sizeof(CUDA_State_t);
	const int source_x = int(SOUND_SOURCE_X / GRID_PRECISION);
	const int source_y = int(SOUND_SOURCE_Y / GRID_PRECISION);
	CUDA_State_t div_xi = 1.0 / (std::sqrt(2.0) * XI);
	// set block and grid dimensions
	const int block_x = 32, block_y = 8;
	dim3 block_size(block_x, block_y);
	dim3 grid_size(grid_width / block_x + 1, grid_height / block_y + 1);

	// create scene in the GPU memory
	CUDA_State old_state, current_state, next_state;
	cudaMalloc(&old_state, n_bytes);
	cudaMalloc(&current_state, n_bytes);
	cudaMalloc(&next_state, n_bytes);
	cudaMemset(old_state, 0x00, n_bytes);
	cudaMemset(current_state, 0x00, n_bytes);
	cudaMemset(next_state, 0x00, n_bytes);
	// prepare host memory
	CUDA_State host_state;
	cudaMallocHost(&host_state, n_bytes, cudaHostAllocDefault);

	// set impulse to the current state
	initState<<< grid_size, block_size >>>(old_state, current_state, next_state, source_x, source_y, grid_width, grid_height);

	for (auto step = 0u; step < N_STEPS; ++step)
	{
		//std::cout << step << "/" << N_STEPS << "\r" << std::flush;
		// compute FDTD on the device
		FDTDGPU<<<grid_size, block_size>>>(old_state, current_state, next_state, grid_width, grid_height, div_xi);
		// take one step forward
		auto temp = old_state;
		old_state = current_state;
		current_state = next_state;
		next_state = temp;
		// load data
		//cudaMemcpy(host_state, current_state, n_bytes, cudaMemcpyDeviceToHost);
	}
	//std::cout << N_STEPS << "/" << N_STEPS << " ...ready." << std::endl;
	cudaFree(old_state);
	cudaFree(current_state);
	cudaFree(next_state);
	cudaFreeHost(host_state);
}


void testFDTDGpuTexture()
{

	// run speedtest on the GPU
	const int grid_width = int(ROOM_WIDTH / GRID_PRECISION);
	const int grid_height = int(ROOM_HEIGHT / GRID_PRECISION);
	const int n_elements = grid_width * grid_height;
	const int n_bytes = n_elements * sizeof(CUDA_State_t);
	const int source_x = int(SOUND_SOURCE_X / GRID_PRECISION);
	const int source_y = int(SOUND_SOURCE_Y / GRID_PRECISION);
	float div_xi = 1.0f / (std::sqrt(2.0f) * XI);
	// set block and grid dimensions
	const int block_x = 32, block_y = 8;
	dim3 block_size(block_x, block_y);
	dim3 grid_size(grid_width / block_x + 1, grid_height / block_y + 1);



	// create scene in the GPU memory
	CUDA_State old_state, current_state, next_state;
	size_t pitch;
	//cudaMallocPitch(&old_state, &pitch, grid_width, grid_height);
	//cudaMallocPitch(&current_state, &pitch, grid_width, grid_height);
	//cudaMallocPitch(&next_state, &pitch, grid_width, grid_height);

	cudaMalloc(&old_state, n_bytes);
	cudaMalloc(&current_state, n_bytes);
	cudaMalloc(&next_state, n_bytes);
	cudaMemset(old_state, 0x00, n_bytes);
	cudaMemset(current_state, 0x00, n_bytes);
	cudaMemset(next_state, 0x00, n_bytes);
	// prepare host memory
	CUDA_State host_state;
	cudaMallocHost(&host_state, n_bytes, cudaHostAllocDefault);

	// set impulse to the current state
	initState<<< grid_size, block_size >>>(old_state, current_state, next_state, source_x, source_y, grid_width, grid_height);

	// create resource descriptors
	cudaResourceDesc resDesc_old;
	memset(&resDesc_old, 0, sizeof(resDesc_old));
	resDesc_old.resType = cudaResourceTypeLinear;
	resDesc_old.res.linear.devPtr = old_state;
	resDesc_old.res.linear.desc.f = cudaChannelFormatKindFloat;
	resDesc_old.res.linear.desc.x = 32; // bits per channel
	resDesc_old.res.linear.sizeInBytes = n_bytes;

	cudaResourceDesc resDesc_current;
	memset(&resDesc_current, 0, sizeof(resDesc_current));
	resDesc_current.resType = cudaResourceTypeLinear;
	resDesc_current.res.linear.devPtr = current_state;
	resDesc_current.res.linear.desc.f = cudaChannelFormatKindFloat;
	resDesc_current.res.linear.desc.x = 32; // bits per channel
	resDesc_current.res.linear.sizeInBytes = n_bytes;

	cudaResourceDesc resDesc_next;
	memset(&resDesc_next, 0, sizeof(resDesc_next));
	resDesc_next.resType = cudaResourceTypeLinear;
	resDesc_next.res.linear.devPtr = next_state;
	resDesc_next.res.linear.desc.f = cudaChannelFormatKindFloat;
	resDesc_next.res.linear.desc.x = 32; // bits per channel
	resDesc_next.res.linear.sizeInBytes = n_bytes;

	// create texture descriptors
	cudaTextureDesc texDesc_old;
	memset(&texDesc_old, 0, sizeof(texDesc_old));
	texDesc_old.readMode = cudaReadModeElementType;

	cudaTextureDesc texDesc_current;
	memset(&texDesc_current, 0, sizeof(texDesc_current));
	texDesc_current.readMode = cudaReadModeElementType;

	cudaTextureDesc texDesc_next;
	memset(&texDesc_next, 0, sizeof(texDesc_next));
	texDesc_next.readMode = cudaReadModeElementType;

	//create texture objects
	cudaTextureObject_t tex_old = 0, tex_current = 0, tex_next = 0;
	cudaCreateTextureObject(&tex_old, &resDesc_old, &texDesc_old, NULL);
	cudaCreateTextureObject(&tex_current, &resDesc_current, &texDesc_current, NULL);
	cudaCreateTextureObject(&tex_next, &resDesc_next, &texDesc_next, NULL);

	for (auto step = 0u; step < N_STEPS; ++step)
	{
		//std::cout << step << "/" << N_STEPS << "\r" << std::flush;
		// compute FDTD on the device
		FDTDGPU_Texture<<<grid_size, block_size >>>(tex_old, tex_current, next_state, grid_width, grid_height, div_xi);
		// take one step forward
		auto temp = tex_old;
		tex_old = tex_current;
		tex_current = tex_next;
		tex_next = temp;
		auto temp2 = old_state;
		old_state = current_state;
		current_state = next_state;
		next_state = temp2;
		// load data
		//cudaMemcpy(host_state, current_state, n_bytes, cudaMemcpyDeviceToHost);
	}
	//std::cout << N_STEPS << "/" << N_STEPS << " ...ready." << std::endl;
	cudaFree(old_state);
	cudaFree(current_state);
	cudaFree(next_state);
	cudaFreeHost(host_state);
}


void testFDTDGpuTexture2D()
{

	// run speedtest on the GPU
	const int grid_width = int(ROOM_WIDTH / GRID_PRECISION);
	const int grid_height = int(ROOM_HEIGHT / GRID_PRECISION);
	const int n_elements = grid_width * grid_height;
	const int n_bytes = n_elements * sizeof(CUDA_State_t);
	const int source_x = int(SOUND_SOURCE_X / GRID_PRECISION);
	const int source_y = int(SOUND_SOURCE_Y / GRID_PRECISION);
	float div_xi = 1.0f / (std::sqrt(2.0f) * XI);
	// set block and grid dimensions
	const int block_x = 32, block_y = 8;
	dim3 block_size(block_x, block_y);
	dim3 grid_size(grid_width / block_x + 1, grid_height / block_y + 1);



	// create scene in the GPU memory
	CUDA_State old_state, current_state, next_state;
	size_t pitch;
	size_t byte_width = grid_width * sizeof(CUDA_State_t);
	cudaMallocPitch(&old_state, &pitch, byte_width, grid_height);
	cudaMallocPitch(&current_state, &pitch, byte_width, grid_height);
	cudaMallocPitch(&next_state, &pitch, byte_width, grid_height);
	cudaMemset2D(old_state, pitch, 0x00, byte_width, grid_height);
	cudaMemset2D(current_state, pitch, 0x00, byte_width, grid_height);
	cudaMemset2D(next_state, pitch, 0x00, byte_width, grid_height);
	// prepare host memory
	CUDA_State host_state;
	cudaMallocHost(&host_state, n_bytes, cudaHostAllocDefault);

	// set impulse to the current state
	initState2D <<< grid_size, block_size >>>(old_state, current_state, next_state, source_x, source_y, pitch, grid_width, grid_height);

	// create resource descriptors
	cudaResourceDesc resDesc_old;
	memset(&resDesc_old, 0, sizeof(resDesc_old));
	resDesc_old.resType = cudaResourceTypePitch2D;
	resDesc_old.res.pitch2D.devPtr = old_state;
	resDesc_old.res.pitch2D.desc.f = cudaChannelFormatKindFloat;
	resDesc_old.res.pitch2D.desc.x = 32; // bits per channel
	resDesc_old.res.pitch2D.pitchInBytes = pitch;

	cudaResourceDesc resDesc_current;
	memset(&resDesc_current, 0, sizeof(resDesc_current));
	resDesc_current.resType = cudaResourceTypePitch2D;
	resDesc_current.res.pitch2D.devPtr = current_state;
	resDesc_current.res.pitch2D.desc.f = cudaChannelFormatKindFloat;
	resDesc_current.res.pitch2D.desc.x = 32; // bits per channel
	resDesc_current.res.pitch2D.pitchInBytes = pitch;

	cudaResourceDesc resDesc_next;
	memset(&resDesc_next, 0, sizeof(resDesc_next));
	resDesc_next.resType = cudaResourceTypePitch2D;
	resDesc_next.res.pitch2D.devPtr = next_state;
	resDesc_next.res.pitch2D.desc.f = cudaChannelFormatKindFloat;
	resDesc_next.res.pitch2D.desc.x = 32; // bits per channel
	resDesc_next.res.pitch2D.pitchInBytes = pitch;

	// create texture descriptors
	cudaTextureDesc texDesc_old;
	memset(&texDesc_old, 0, sizeof(texDesc_old));
	texDesc_old.readMode = cudaReadModeElementType;

	cudaTextureDesc texDesc_current;
	memset(&texDesc_current, 0, sizeof(texDesc_current));
	texDesc_current.readMode = cudaReadModeElementType;

	cudaTextureDesc texDesc_next;
	memset(&texDesc_next, 0, sizeof(texDesc_next));
	texDesc_next.readMode = cudaReadModeElementType;

	//create texture objects
	cudaTextureObject_t tex_old = 0, tex_current = 0, tex_next = 0;
	cudaCreateTextureObject(&tex_old, &resDesc_old, &texDesc_old, NULL);
	cudaCreateTextureObject(&tex_current, &resDesc_current, &texDesc_current, NULL);
	cudaCreateTextureObject(&tex_next, &resDesc_next, &texDesc_next, NULL);

	for (auto step = 0u; step < N_STEPS; ++step)
	{
		//std::cout << step << "/" << N_STEPS << "\r" << std::flush;
		// compute FDTD on the device
		FDTDGPU_Texture2D<<<grid_size, block_size >>>(tex_old, tex_current, next_state, grid_width, grid_height, div_xi);
		// take one step forward
		auto temp = tex_old;
		tex_old = tex_current;
		tex_current = tex_next;
		tex_next = temp;
		auto temp2 = old_state;
		old_state = current_state;
		current_state = next_state;
		next_state = temp2;
		// load data
		//auto err = cudaMemcpy2D(host_state, byte_width, current_state, pitch, byte_width, grid_height, cudaMemcpyDeviceToHost);
		//cudaMemcpy(&host_state[0], current_state, n_bytes, cudaMemcpyDeviceToHost);
	}
	//std::cout << N_STEPS << "/" << N_STEPS << " ...ready." << std::endl;
	cudaFree(old_state);
	cudaFree(current_state);
	cudaFree(next_state);
	cudaFreeHost(host_state);
}


void testFDTDGpuManaged()
{

	// run speedtest on the GPU
	const int grid_width = int(ROOM_WIDTH / GRID_PRECISION);
	const int grid_height = int(ROOM_HEIGHT / GRID_PRECISION);
	const int n_elements = grid_width * grid_height;
	const int n_bytes = n_elements * sizeof(CUDA_State_t);
	const int source_x = int(SOUND_SOURCE_X / GRID_PRECISION);
	const int source_y = int(SOUND_SOURCE_Y / GRID_PRECISION);
	CUDA_State_t div_xi = 1.0 / (std::sqrt(2.0) * XI);
	// set block and grid dimensions
	const int block_x = 32, block_y = 8;
	dim3 block_size(block_x, block_y);
	dim3 grid_size(grid_width / block_x + 1, grid_height / block_y + 1);

	// create scene in the unified memory
	CUDA_State old_state, current_state, next_state;
	cudaMallocManaged(&old_state, n_bytes);
	cudaMallocManaged(&current_state, n_bytes);
	cudaMallocManaged(&next_state, n_bytes);
	cudaMemset(old_state, 0x00, n_bytes);
	cudaMemset(current_state, 0x00, n_bytes);
	cudaMemset(next_state, 0x00, n_bytes);

	// set impulse to the current state
	initState <<< grid_size, block_size >>>(old_state, current_state, next_state, source_x, source_y, grid_width, grid_height);

	for (auto step = 0u; step < N_STEPS; ++step)
	{
		//std::cout << step << "/" << N_STEPS << "\r" << std::flush;
		// compute FDTD on the device
		FDTDGPU <<<grid_size, block_size >>>(old_state, current_state, next_state, grid_width, grid_height, div_xi);
		cudaDeviceSynchronize();
		// take one step forward
		auto temp = old_state;
		old_state = current_state;
		current_state = next_state;
		next_state = temp;
		// force copy data to host
		volatile CUDA_State_t el1 = current_state[0];
		volatile CUDA_State_t el2 = current_state[1];
		volatile CUDA_State_t el3 = current_state[2];
	}
	//std::cout << N_STEPS << "/" << N_STEPS << " ...ready." << std::endl;
	cudaFree(old_state);
	cudaFree(current_state);
	cudaFree(next_state);
}
